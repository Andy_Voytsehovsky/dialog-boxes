package project.voytsehovsky.andy.dialogboxes;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button bntShow;
    private Button bntAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addDialogBoxes();
    }

    public void addDialogBoxes(){
        bntShow = (Button)findViewById(R.id.button);
        bntAlert = (Button)findViewById(R.id.button2);

        bntAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder aBuilder = new AlertDialog.Builder(MainActivity.this);
                aBuilder.setMessage("Вы хотите закрыть приложение")
                    .setCancelable(false)

                    .setPositiveButton("Да", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        finish();
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        dialog.cancel();
                    }
                });
                AlertDialog alert = aBuilder.create();
                alert.setTitle("Закрытие приложенния");
                alert.show();
            }
        });
    }
}
